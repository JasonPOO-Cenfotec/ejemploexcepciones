package cr.ac.ucenfotec.ui;

public class UI {

    public static void main(String[] args) {
        int x =0;
        int[] listaEdades = new int[3];
        System.out.println("Paso 1: Inicia el programa....");
        try {
            x = 15 / 0;
            System.out.println("Paso 2: Se imprime el valor de x = " + x);
            System.out.println("Paso 3: Se crea la lista de edades....");
            listaEdades[0] = 25;
            listaEdades[1] = 40;
            listaEdades[2] = 15;
            listaEdades[3] = 65;
        }catch(Exception e){
            System.out.println("No se logró realizar la transacción: " + e.getMessage());
        }finally {
            System.out.println("Yo siempre me ejecutó exista error o no!");
        }

        System.out.println("Paso 4: Se imprime la lista de edades....");
        for (int i = 0; i < listaEdades.length; i++) {
            System.out.println((i+1) + " - edad: " + listaEdades[i]);
        }
        System.out.println("Paso 5: Fin del programa....");
    }
}
