package cr.ac.ucenfotec.ui;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.Buffer;

public class UI2 {
    private static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    private static String nombre;
    private static int edad;

    public static void main(String[] args) throws Exception{
        solicitarDatos();
        imprimirDatos();
    }

    public static void solicitarDatos() throws Exception{
        System.out.print("Por favor digite el nombre: ");
        nombre = in.readLine();
        System.out.print("Por favor digite la edad: ");
        edad = Integer.parseInt(in.readLine());
    }

    public static void imprimirDatos(){
        System.out.println("El nombre es  " + nombre + ", la edad es " + edad);
    }
}
