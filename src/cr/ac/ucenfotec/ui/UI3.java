package cr.ac.ucenfotec.ui;

import cr.ac.ucenfotec.bl.DivisionException;

public class UI3 {

    public static void main(String[] args) {
        try {
            calcularDivision();
        }catch (DivisionException e){
            System.out.println("Se presentó un error en el sistema: " + e.getMessage());
        }
    }

    public static void calcularDivision() throws DivisionException {
        int x = 10;
        int y = 0;

        // Podemos generar una excepción controlada
        // donde podemos indicar un mensaje de error más claro para el usuario
        if (y == 0){
            throw new DivisionException("Error, el divisor no puede ser cero!");
        }

        int resultado = x/y;
        System.out.println("El resultado de la división es: " + resultado);
    }

}
