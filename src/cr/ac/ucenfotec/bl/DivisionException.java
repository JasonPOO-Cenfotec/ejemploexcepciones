package cr.ac.ucenfotec.bl;

public class DivisionException extends Exception{

    public DivisionException(String mensaje){
        super(mensaje);
    }
}
